<?php
/**
 * Displays the footer widget area
 *
 * @package WordPress
 * @subpackage Fayetteville_Doulas
 * @since 1.0.0
 */
?>

<div class="footer-top">

	<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>

		<div class="widget-area footer-left" role="complementary" aria-label="<?php esc_attr_e( 'Footer Left', 'fay-doulas' ); ?>">
			<h2 class="section-title"><?php esc_html_e( 'Discover More' ); ?></h2>
			<?php
			if ( is_active_sidebar( 'footer-1' ) ) {
				?>
				<div class="widget-column footer-widget-1">
					<?php dynamic_sidebar( 'footer-1' ); ?>
				</div>
				<?php
			}
			?>
		</div><!-- .widget-area -->

	<?php endif; ?>

	<div class="widget-area footer-right" role="complementary" aria-label="<?php esc_attr_e( 'Footer Right', 'fay-doulas' ); ?>">
		<h2 class="section-title"><?php esc_html_e( 'Contact Us' ); ?></h2>
		<?php get_template_part( 'template-parts/footer/contact-info' ); ?>
		<?php
		if ( is_active_sidebar( 'footer-2' ) ) {
			?>
			<div class="widget-column footer-widget-2">
				<?php dynamic_sidebar( 'footer-2' ); ?>
			</div>
			<?php
		}
		?>

		<nav class="social-navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'fay-doulas' ); ?>">
			<?php (new RDBI\ThemeSetup\RegisterNavigation())->rdbiSocialNav(); ?>
		</nav><!-- .social-navigation -->

	</div><!-- .widget-area -->

</div>
