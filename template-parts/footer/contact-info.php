<?php
/**
 * Displays the contact info from Customizer
 *
 * @package WordPress
 * @subpackage Fayetteville_Doulas
 * @since 1.0.0
 */

?>

<div class="contact-info">
	<h3 class="company-name">
		<?php echo get_theme_mod ( 'rdbi_company_name' ); ?>
	</h3>
	<p>
		<?php
		$address1 = get_theme_mod ( 'rdbi_street_address_1' );
		$address2 = get_theme_mod ( 'rdbi_street_address_2' );
		$city = get_theme_mod ( 'rdbi_city' );
		$state = get_theme_mod ( 'rdbi_state' );
		$zip = get_theme_mod ( 'rdbi_zip' );
		$phone = get_theme_mod ( 'rdbi_phone' );
		$fax = get_theme_mod ( 'rdbi_fax' );
		$email = get_option ( 'admin_email' );

		$full_address = array (
				$address1, $address2, $city, $state, $zip
		);
		?>
		<?php if ( ! empty( $full_address ) ) : ?>
		<a target="_blank" href="https://www.google.com/maps/place/<?php echo esc_html( preg_replace( '/\s+/', '+', $full_address ) ); ?>">
			<?php if ( $address1 ) {
				echo $address1;
				echo '<br>';
			} ?>
			<?php if ( $address2 ) {
				echo $address2;
				echo '<br>';
			} ?>
			<?php if ( $city ) { echo $city; } ?><?php esc_html_e( ', ' ); ?><?php if ( $state ) { echo $state; } ?> <?php if ( $zip ) { echo $zip; } ?>
		</a>
		<?php endif; ?>
	</p>
	<p>
		<?php if ( $phone ) : ?>
			<?php esc_html_e( 'Phone: ' ); ?>
			<a href="tel:<?php echo esc_html( preg_replace( '/\D+/', '', $phone ) ); ?>"><?php echo $phone; ?></a><br>
		<?php endif; ?>
		<?php if ( $fax ) : ?>
			<?php esc_html_e( 'Fax: ' ); ?>
			<a href="tel:<?php echo esc_html( preg_replace( '/\D+/', '', $fax ) ); ?>"><?php echo $fax; ?></a><br>
		<?php endif; ?>
		<?php if ( $email ) : ?>
			<?php esc_html_e( 'Email: ' ); ?>
			<a href="mailto:<?php echo antispambot( $email ); ?>"><?php echo $email; ?></a>
		<?php endif; ?>
	</p>
	<div class="wp-block-button is-style-outline">
		<a href="<?php echo get_permalink( 751 ); ?>" class="wp-block-button__link has-text-color has-secondary-color has-background has-white-background-color"><?php esc_html_e( 'Send us a message!' ); ?>
		</a>
	</div>
</div>
