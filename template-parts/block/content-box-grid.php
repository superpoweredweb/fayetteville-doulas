<?php
/**
 * Block Name: Circle Image Grid
 *
 * This is the template that displays the circle image grid block.
 */

// create id attribute for specific styling
$id = 'box-grid-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

if ( have_rows( 'box_grid' ) ) : ?>

<div id="<?php echo $id; ?>" class="box-grid <?php echo $align_class; ?>">

	<h2 class="box-grid-title"><?php the_field( 'section_title' ); ?></h2>

	<ul  class="box-grid-list">

		<?php while ( have_rows( 'box_grid' ) ) : the_row();

			$link 		= get_sub_field( 'link' );
			$title		= get_sub_field( 'title' );
			$content	= get_sub_field( 'content' );

		?>

			<li class="box-grid-item">

				<?php if( !empty( $title )):?>
					<h3 class="box-grid-item-title"><?php esc_html_e( $title ); ?></h3>
				<?php endif; ?>

				<?php if( !empty( $content )) {

					echo wp_kses_post( $content );

				};?>

				<?php if( !empty( $link )) {
					echo '<a href="' . esc_url( $link['url'] ) . '" class="circle-grid-link">';
					echo esc_html( $link['title'] ) . '</a>';
				}; ?>

			</li>

		<?php endwhile; ?>

	</ul>

</div>

<?php endif; ?>
