<?php
/**
 * Block Name: Circle Image Grid
 *
 * This is the template that displays the circle image grid block.
 */

// create id attribute for specific styling
$id = 'circle-grid-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

if ( have_rows( 'circle_image_grid' ) ) : ?>

<div id="<?php echo $id; ?>" class="circle-grid <?php echo $align_class; ?>">

	<h2 class="circle-grid-title"><?php the_field( 'section_title' ); ?></h2>

	<ul  class="circle-grid-list">

		<?php while ( have_rows( 'circle_image_grid' ) ) : the_row();

			$link 		= get_sub_field( 'link' );
			$image 		= get_sub_field( 'image' );
			$title		= get_sub_field( 'title' );
			$content	= get_sub_field( 'content' );

			//cropped image for circle container
			$size		= 'rdbi-circle';

		?>

			<li class="circle-grid-item">

				<?php if( !empty( $image )): ?>
					<div class="circle-grid-image">
						<?php echo wp_get_attachment_image( $image, $size ); ?>
					</div>
				<?php endif; ?>

				<?php if( !empty( $title )):?>
					<h3 class="circle-grid-item-title"><?php esc_html_e( $title ); ?></h3>
				<?php endif; ?>

				<?php if( !empty( $content )) {

					echo wp_kses_post( $content );

				};?>

				<?php if( !empty( $link )) {
					echo '<a href="' . esc_url( $link['url'] ) . '" class="circle-grid-link">';
					echo esc_html( $link['title'] ) . '</a>';
				}; ?>

			</li>

		<?php endwhile; ?>

	</ul>

</div>

<?php endif; ?>
