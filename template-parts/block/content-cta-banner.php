<?php
/**
 * Block Name: CTA Banner
 *
 * This is the template that displays the call-to-action banner block.
 */

// create id attribute for specific styling
$id = 'cta-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

$link = get_field( 'link' );

?>
<div id="<?php echo $id; ?>" class="cta-banner <?php echo $align_class; ?>">
	<span class="wp-block-media-overlay">
		<header class="cta-header">
			<h2 class="cta-title"><?php the_field( 'header' ); ?></h2>
		</header>
		<div class="cta-content">
			<?php the_field('body'); ?>
			<?php if ( $link ) : ?>
				<?php
				$link_url = $link['url'];
				$link_title = $link['title'];
				?>
				<div class="wp-block-button is-style-solid">
				<a class="wp-block-button__link has-text-color has-white-color has-background has-secondary-background-color is-style-square" href="<?php echo esc_url($link_url); ?>"><?php echo esc_html($link_title); ?></a>
			</div>
			<?endif; ?>
		</div>
	</span>
</div>

