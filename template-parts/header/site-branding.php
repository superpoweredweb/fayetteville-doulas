<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Fayetteville_Doulas
 * @since 1.0.0
 */
?>
<div class="site-branding">

	<div class="logo">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/lib/img/logo.png" alt="The Fayetteville Doulas Logo" class="logo-img">
		</a>
	</div>
	<?php if ( ! empty( $blog_info ) ) : ?>
		<?php if ( is_front_page() && is_home() ) : ?>
			<h1 class="site-title screen-reader-text"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		<?php else : ?>
			<p class="site-title screen-reader-text"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
		<?php endif; ?>
	<?php endif; ?>

	<? /* php
	$description = get_bloginfo( 'description', 'display' );
	if ( $description || is_customize_preview() ) :
		?>
			<p class="site-description">
				<?php echo $description; ?>
			</p>
	<?php endif; */ ?>
	<div class="site-menu-container">
		<div class="site-menu-inner-container">
			<button id="menu-toggle" class="menu-toggle">
				<?php
				_e( 'Menu', 'fay-doulas' );
				$icon = twentynineteen_get_icon_svg( 'arrow_drop_down_ellipsis', 22 );
				echo $icon;
				?>
			</button>
			<div id="site-header-menu" class="site-header-menu">
				<nav id="site-navigation" class="main-navigation" aria-label="<?php esc_attr_e( 'Main Menu', 'fay-doulas' ); ?>">
					<?php (new RDBI\ThemeSetup\RegisterNavigation())->rdbiPrimaryNav(); ?>
				</nav><!-- #site-navigation -->
			</div>
		</div>
	</div>
</div><!-- .site-branding -->
