<?php
/**
 * Template part for displaying a blog grid list with featured image
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Fayetteville_Doulas
 * @since 1.0.0
 */

?>
<?php
// example args
$args = array( 'posts_per_page' => 3 );

// the query
$the_query = new WP_Query( $args );
?>

<div class="section-header">
	<h2 class="section-title">
		<?php
		$blogid = get_option( 'page_for_posts' );
		echo get_the_title( $blogid );
		?></h2>
</div>

<?php if ( $the_query->have_posts() ) : ?>

<div class="content-inner">

	<ol class="post-list">

		<!-- start of the loop -->
		<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

		<li class="post-item">

			<?php get_template_part( 'template-parts/content/content-excerpt' ); ?>

		</li>

		<?php endwhile; ?><!-- end of the loop -->

		<!-- put pagination functions here -->
		<?php wp_reset_postdata(); ?>

		<?php else:  ?>

		<p><?php get_template_part( 'template-parts/content/content', 'none' ); ?></p>

	</ol>

	<?php endif; ?>

</div>

